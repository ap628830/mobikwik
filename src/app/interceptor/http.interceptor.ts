import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

@Injectable()
export class CacheErrorInterceptorService implements HttpInterceptor {
    constructor(private messageService: MessageService) {}
   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
       return next.handle(request).pipe(
           catchError(response=>{
            this.messageService.add({severity:'error', summary:'Error', detail:"Server is down ,please start the server.\ncommand: 'npm run json-server'"});

               return throwError(response)
           })
       )
       // All HTTP requests are going to go through this method
   }
}
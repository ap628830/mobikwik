import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'creditDebitMaskPipe'
})
export class CreditDebitMaskPipePipe implements PipeTransform {
  transform(cardNumber: string, visibleDigits: number): string {
    let visibleFrontNumber = cardNumber.slice(0,visibleDigits)
    let maskedNumbers1 = cardNumber.slice(visibleDigits, 2*visibleDigits);
    let maskedNumbers2 = cardNumber.slice(2*visibleDigits,-visibleDigits)
    let visibleBackNumbers = cardNumber.slice(-visibleDigits);
    return visibleFrontNumber+' '+maskedNumbers1.replace(/./g, '*')+' '+ maskedNumbers2.replace(/./g, '*')+' '+ visibleBackNumbers;
  }
}
import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'creditDebitTypePipe',
})
export class CreditDebitTypePipe implements PipeTransform {
  transform(cardType: string) {
    switch (cardType) {
      case 'visa':
        return '../../../assets/images/visa.svg';
      case 'amex':
        return '../../../assets/images/amex.png';
      case 'dinersclub':
        return '../../../assets/images/diners.svg';
      case 'discover':
        return '../../../assets/images/discover.png';
      case 'maestro':
        return '../../../assets/images/maestro.svg';
      case 'mastercard':
        return '../../../assets/images/mastercard.svg';
      case 'jcb':
        return '../../../assets/images/jcb.svg';
      case 'forbrugsforeningen':
        return '../../../assets/images/forbrugsforeningen.png';
      case 'dankort':
        return '../../../assets/images/dankort.svg';
      case 'unionpay':
        return '../../../assets/images/unionpay.png';
    }
    return '../../../assets/images/credit-card.png';
  }
}

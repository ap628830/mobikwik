import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MainService } from 'src/app/main.service';
import { Card } from '../card';
import { ConfirmationService, MessageService } from 'primeng/api';
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  cardData$: Observable<Card>
  openForm: boolean

  constructor(private mainService: MainService, private confirmationService: ConfirmationService,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.cardData$= this.getSortedData()
  }

  getSortedData():Observable<Card> {
    return this.mainService.getData().pipe(
      map(data=>{
          data.sort((a,b)=>(a.cardNumber>b.cardNumber)?1:-1)
          return data
      })
    )
  }

  removeCard(id:number){
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this card?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.mainService.removeData(id).subscribe(next=>{
          this.cardData$=this.getSortedData()
          this.messageService.add({severity:'success', summary:'Success', detail:'Card deleted'});
        },error=>{
          this.messageService.add({severity:'error', summary:'Error', detail:"Card can't be deleted"});

        })
          
      }
  });
  
  }
  saveData(){
    this.messageService.add({severity:'success', summary:'Success', detail:'Card Added'});
    this.cardData$ = this.getSortedData()
  }

  addNewCard():void{
    this.openForm = true
    this.mainService.displayForm = true
  }


}

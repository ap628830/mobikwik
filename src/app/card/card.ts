export interface Card {
  [x: string]: any;
  cardNumber?: string;
  expiryMonth?: string;
  expiryYear?: string;
  cvv?: string;
  cardType?: string;
}

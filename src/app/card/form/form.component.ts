import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MainService } from 'src/app/main.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { CreditCardValidators } from 'angular-cc-library';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  constructor(
    public mainService: MainService,
    private fb: FormBuilder,
    private messageService: MessageService
  ) {}

  @Output() save = new EventEmitter();

  cardData: string[] = [];
  months: string[] = [];
  years: string[] = [];
  today: Date = new Date();
  currentMonth: number;
  currentYear: number;
  checkAmex: boolean;
  cardDetailForm = this.fb.group({
    cardNumber: ['', [CreditCardValidators.validateCCNumber]],
    expiryMonth: ['', [Validators.required]],
    expiryYear: ['', [Validators.required]],
    cvv: [
      '',
      [Validators.required, Validators.minLength(3), Validators.maxLength(4)],
    ],
    cardType: [''],
  });

  ngOnInit(): void {
    this.currentMonth = this.today.getMonth();
    this.currentYear = this.today.getFullYear();
    for (let i = 1; i <= 12; i++) {
      if (i < 10) {
        this.months.push('0' + i);
      } else this.months.push(i.toString());
    }
    for (let i = 1; i < 50; i++)
      this.years.push((this.currentYear + i - 1).toString());

    this.getCardData();
  }

  getCardData() {
    this.mainService.getData().subscribe((response: any) => {
      response.forEach((card) => {
        this.cardData.push(card.cardNumber);
      });
    });
  }
  get formControl() {
    return this.cardDetailForm.controls;
  }
  getCardType(ccNumber) {
    ccNumber.resolvedScheme$.subscribe((cardValue) => {
      this.formControl.cardType.setValue(cardValue);
    });
  }
  get validateCardType() {
    const cvvValue = this.formControl.cvv.value;
    if (cvvValue && cvvValue.length == 4) {
      if (this.formControl.cardType.value == 'amex') return false;
      return true;
    }
    return false;
  }
  addData() {
    const cardObject = this.cardDetailForm.value;
    cardObject.cardNumber = cardObject.cardNumber.replace(/ /g, '');
    const cardNumber = cardObject.cardNumber;
    if (this.cardData.indexOf(cardNumber) != -1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warning',
        detail: 'Card already exist',
      });
      return;
    }
    if (+cardObject.expiryYear <= this.currentYear) {
      if (+cardObject.expiryMonth < this.currentMonth + 1) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warning',
          detail: 'Card Expired',
        });
        return;
      }
    }
    this.mainService.displayForm = false;
    this.mainService.addData(this.cardDetailForm.value);
    this.save.emit();
    this.cardDetailForm.reset();
    this.getCardData();
  }
}

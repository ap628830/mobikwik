import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FormComponent } from './form/form.component';
import { DialogModule } from 'primeng/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditDebitMaskPipePipe } from './pipe/credit-debit-mask-pipe';
import { MatSelectModule } from '@angular/material/select';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { CreditDebitTypePipe } from './pipe/credit-debit-type-pipe';


@NgModule({
  declarations: [CardComponent,
    FormComponent,CreditDebitMaskPipePipe,
    CreditDebitTypePipe
  ],
  imports: [
    CommonModule,
    InputTextModule,
    ConfirmDialogModule,
    ToastModule,
    DialogModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    CreditCardDirectivesModule
  ],
  providers:[ConfirmationService, MessageService],
  exports: [CardComponent]
})
export class CardModule { }

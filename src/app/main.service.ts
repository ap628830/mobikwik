import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Card } from './card/card';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  url:string = 'http://localhost:3000/card-details/'
  displayForm:boolean
  constructor(private http: HttpClient) { }
 
  getData():Observable<Card>{
    return this.http.get(this.url)
  }

  removeData(id:number):Observable<any> {
    return this.http.delete(this.url+id)
  }

  addData(data:Card):void {
    this.http.post(this.url,data).subscribe()
  }
}
